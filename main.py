import PySimpleGUI as sg
import os
import json
import pandas as pd


class CsvData:
    FILTERED_CSV = None
    FRONTEND_STOCKS = None

    def __init__(self, path, etf_db):
        # Grab relevant columns only and remove "Transactions" and other junk
        self.FILTERED_CSV = pd.read_csv(path, usecols=['Action', 'Name', 'Total (EUR)']).dropna(subset=['Name'])
        # Remove Dividend rows (if necessary)
        self.FILTERED_CSV = self.FILTERED_CSV[~self.FILTERED_CSV['Action'].str.contains('ividend')]
        # Drop duplicate names
        self.FRONTEND_STOCKS = self.FILTERED_CSV['Name'].drop_duplicates()
        # Removes ETFs from stock overview (if necessary)
        self.FRONTEND_STOCKS = self.FRONTEND_STOCKS[~self.FRONTEND_STOCKS.isin(etf_db)]

    def calculate_tax(self, instruments, rate):
        filtered_instruments = self.FILTERED_CSV[self.FILTERED_CSV['Name'].isin(instruments)]  # Filter

        instruments_len = len(filtered_instruments.index)
        instruments_basis = filtered_instruments['Total (EUR)'].sum()
        instruments_amount = (rate * filtered_instruments['Total (EUR)'].sum()) / 100.0

        return {'instruments_len': instruments_len, 'instruments_basis': instruments_basis, 'instruments_amount':
                instruments_amount}

    def create_csv(self, path, stocks, etfs):
        csv_instruments = pd.DataFrame(data={
            'Stocks': [stocks['instruments_len'], stocks['instruments_basis'], stocks['instruments_amount']],
            'ETFs': [etfs['instruments_len'], etfs['instruments_basis'], etfs['instruments_amount']]},
            index=['Amount of:', 'Tax Basis:', 'Tax Amount:'])
        csv_instruments.to_csv(path)


class EtfDatabase:
    DIR_PATH = None
    DB_PATH = None

    def __init__(self):
        self.DIR_PATH = f"{os.environ['APPDATA']}\\t212-simpletob\\"
        self.DB_PATH = f"{self.DIR_PATH}/etfdb.json"
        if not os.path.exists(self.DIR_PATH):
            os.makedirs(self.DIR_PATH)
            with open(self.DB_PATH, "w") as f:
                f.close()

    def db_read(self):
        with open(self.DB_PATH) as f:
            try:
                data = json.load(f)
                f.close()
                return data
            except ValueError:
                return []

    def db_write(self, etfs):
        with open(self.DB_PATH, 'w+') as f:
            json.dump(etfs, f)
            f.close()


# GUI Structure
sg.theme('Darkbrown1')

sg.popup("SimpleTOB calculates exact rates of 0.35% and 0.12% respectively for stocks and ETFs. This may not always "
         "be correct.", title="Disclaimer")

layout = [[sg.Text('Welcome, start by importing your T212 .csv file.')],
          [sg.FileBrowse(file_types=(('.csv files', '.csv'),), target='-IMPORT-PATH-'),
           sg.Input(key='-IMPORT-PATH-', enable_events=True)],
          [sg.Button('Load CSV')],
          [sg.Text('Stocks:', pad=(10, 10)), sg.Text('ETFs:', pad=(210, 10))],
          [sg.Listbox(values=[], select_mode='extended', size=(25, 15), key='-STOCKS-'),
           sg.Button('<-'), sg.Button('->'),
           sg.Listbox(values=[], select_mode='extended', size=(25, 15), key='-ETFS-')],
          [sg.FileSaveAs(file_types=(('csv file', '*.csv'),), target='-SAVE-PATH-'),
           sg.Input(key='-SAVE-PATH-', enable_events=True)],
          [sg.Button('Calculate!'), sg.Button('Exit')]]

window = sg.Window('T212 - Simple TOB', layout)

# Event Handler
while True:
    event, values = window.read()

    current_stocks = list(window.Element('-STOCKS-').Widget.get(0, last='end'))
    current_etfs = list(window.Element('-ETFS-').Widget.get(0, last='end'))

    if event == sg.WIN_CLOSED or event == "Exit":
        break
    # Initialize classes
    DB = EtfDatabase()
    CSV = CsvData(values['-IMPORT-PATH-'], DB.db_read())

    if event == "Load CSV":
        window['-STOCKS-'](CSV.FRONTEND_STOCKS)
        window['-ETFS-'](DB.db_read())
    if event == "<-":
        new = (values['-ETFS-'])
        window['-STOCKS-'](current_stocks + new)
        window['-ETFS-'](list(set(current_etfs) - set(new)))
    if event == "->":
        new = (values['-STOCKS-'])
        window['-ETFS-'](current_etfs + new)
        window['-STOCKS-'](list(set(current_stocks) - set(new)))
    if event == "Calculate!":
        DB.db_write(current_etfs)
        stocks_tax = CSV.calculate_tax(current_stocks, 0.12)
        etfs_tax = CSV.calculate_tax(current_etfs, 0.35)
        CSV.create_csv(values['-SAVE-PATH-'], stocks_tax, etfs_tax)

        sg.popup("Successfully calculated your TOB declaration!", title="Success!")
