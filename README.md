<h1 align="center">T212 - Simple TOB</h1>
<h3 align="center">Fast and easy TOB declaration, Stocks + ETFs supported.</h3>

---
## Why Simple TOB?
Let Simple TOB do your calculations for you! Create an easy to read CSV file with all the information you need to fill
in your declaration.

### Features:
- Easy to use GUI
- Separated calculation of amount of Stocks and/or ETFs
- Separated calculation of Tax base amount for Stocks and/or ETFs
- Separated calculation of definitive tax amount for Stocks and/or ETFs (the different rates are no problem!)
- Stores your ETFs in a local database, so you only have to categorize new ETFs once!

### How to use:
First export your history from T212. <b>(Since V1.1.0 you can export everything, it filters out the unnecessary entries.)</b>
Then download the latest release from this Github repo and run it.

The app is made for Windows because of its large userbase. But as a GNU/Linux user myself you can easily run it with Wine.
Wine is also available on Mac, but I don't know how well it works. (Let me know!)

### How to contribute:
If you have new ideas for the app you can create a new feature request under the Issues, or fork it and code it yourself!
Run ```pip install -r required_libraries.txt``` to install the necessary Python modules.
Create a pull request afterwards, and I'll look into it :)

If you encounter bugs please create a bug report!

### Disclaimer:
After talking about this program on Reddit I became aware that the TOB wasn't as easy as I thought. (it never is huh...)
This program calculates a rate of 0.35% in the first column and a rate of 0.12% in the second. It is possible that this isn't always correct as pointed out [here](https://www.reddit.com/r/BEFire/comments/otwxrk/i_created_a_program_to_calculate_your_tob_when/h76ko77?utm_source=share&utm_medium=web2x&context=3). Use this program at your own risk.
